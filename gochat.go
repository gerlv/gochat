package main

import (
  "fmt"
  "flag"
  "gitlab.com/gerlv/gochat/server"
  "gitlab.com/gerlv/gochat/client"
)

func main()  {
  fmt.Println("hello world")

  clientPtr := flag.Bool("client", true, "If you want to run a client")
  flag.Parse()

  where := "localhost:8888"

  if *clientPtr == true {
    client.Start(where)
  } else {
    server.Start(where)
  }
}
