Go chat
============

Improving golang skills. Build the following to practice golang.

1. Implement TCP/IP chat (server and a client)
2. Add logging
3. Add encryption (self signed)
4. Add basic web chat
5. Add web API
6. Store messages in a database
7. Integrate `/` commands, weather, etc
8. Add channels
9. Add notifications
