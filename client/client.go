package client

import (
  "net"
  "fmt"
  "os"
  "io"
  "bufio"
  "time"
)

func handleIncomming(conn net.Conn) {
  for {
    message, err := bufio.NewReader(conn).ReadString('\n')
    if err != nil {
      if err == io.EOF {
        conn.Close()
      }
      fmt.Println(err)
    }

    fmt.Println(message)
  }
}

func handleInput(conn net.Conn)  {
  reader := bufio.NewReader(os.Stdin)

  for {
    message, err := reader.ReadString('\n')
    if err != nil {
      panic(err)
    }
    fmt.Fprintf(conn, message)
  }
}

func Start(where string)  {
  fmt.Println("Starting client")

  conn, err := net.DialTimeout("tcp", where, 10*time.Second)
  defer conn.Close()

  if err != nil {
    panic(err)
  }

  go handleIncomming(conn)
  handleInput(conn)
}
