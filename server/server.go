package server

import (
  "net/textproto"
  "io"
  "bufio"
  "net"
  "fmt"
)

var connections []net.Conn

func handleConnection(conn net.Conn) {
  fmt.Println("We've got a new connection")

  fmt.Fprintf(conn, "Knock, knock, Neo.\n")
  fmt.Fprintf(conn, "  (~\\       _\n")
  fmt.Fprintf(conn, "   \\ \\     / \\\n")
  fmt.Fprintf(conn, "    \\ \\___/ /\\\\\n")
  fmt.Fprintf(conn, "     | , , |  ~\n")
  fmt.Fprintf(conn, "     ( =v= )\n")
  fmt.Fprintf(conn, "      ` ^ '\n")

  fmt.Println("Sent hello")

  reader := bufio.NewReader(conn)
  tp := textproto.NewReader(reader)

  for {
    line, err := tp.ReadLine()
    if err != nil {
      if err == io.EOF {
        fmt.Println("Connection closed")
        removeConn(conn)
        broadcast(conn, "User left the building.")
      }
      break
    }

    if len(line) > 0 {
      fmt.Println(line)
      broadcast(conn, line)
    }
  }
}

func broadcast(sender net.Conn, message string) {
  for _, c := range connections {
    if c != sender {
      fmt.Fprintln(c, message)
    }
  }
}

func removeConn(conn net.Conn) {
  // this is very bad!

  for i, c := range connections {
    if c == conn {
      connections = append(connections[:i], connections[i+1:]...)
      break
    }
  }
}

func Start(port string) {
  ln, err := net.Listen("tcp", port)
  if err != nil {
    panic(err)
  }

  for {
    conn, err := ln.Accept()
    defer conn.Close()

    if err != nil {
      fmt.Println(err)
    }

    connections = append(connections, conn) // bad

    go handleConnection(conn)
  }
}
